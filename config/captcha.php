<?php

return [
    'secret' => env('NOCAPTCHA_SECRET'),
    'sitekey' => env('NOCAPTCHA_SITEKEY'),
    'options' => [
        'timeout' => 30,
    ],
    'default'   => [
        'length'    => 5,
        'width'     => 200,
        'height'    => 50,
        'quality'   => 90,
        'math'      => false, //Enable Math Captcha
    ],
];
