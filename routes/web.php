<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.main');
});

Route::get('/user/signin', function () {
    return view('pages.autentikasi.signin');
});

Route::get('/user/signup', function () {
    return view('pages.autentikasi.signup');
});

Route::get('/user/verification', function () {
    return view('pages.autentikasi.verification');
});
Route::get('/user/changepassword', function () {
    return view('pages.autentikasi.changepassword');
});
Route::get('/user/forgotpassword', function () {
    return view('pages.autentikasi.forgotpassword');
});

Route::get('/user/home', function () {
    return view('pages.user.home');
});
Route::get('/user/contactsales', function () {
    return view('pages.user.contactsales');
});

Route::get('/user/profile', function () {
    return view('pages.user.profile');
});
Route::get('/user/team', function () {
    return view('pages.user.team');
});
Route::get('/user/time', function () {
    return view('pages.user.time');
});
Route::get('/user/employee', function () {
    return view('pages.user.employee');
});
Route::get('/user/activity', function () {
    return view('pages.user.activity');
});

Route::get('/user/reward', function () {
    return view('pages.user.reward');
});

Route::get('/user/history', function () {
    return view('pages.user.history');
});
Route::get('/user/users', function () {
    return view('pages.user.users');
});
Route::get('/user/role', function () {
    return view('pages.user.role');
});
Route::get('/user/dashboard', function () {
    return view('includes.dashboard.dashboard');
});
Route::get('/page/moreabout', function () {
    return view('pages.more-pages.more-about');
});
Route::get('/page/moreservices', function () {
    return view('pages.more-pages.more-services');
});

// [your site path]/Http/routes.php


Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
