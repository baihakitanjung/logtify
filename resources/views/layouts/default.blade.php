<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Logtify</title>
    <meta content="" name="descriptison">
    <meta content="" name="keywords">
    <meta name="Description" CONTENT="Logtify adalah solusi aplikasi produktivitas bisnis yang tepat bagi perusahaan anda dari beberapa
                    kendala yang ada saat
                    ini baik untuk karyawan yang bekerja di kantor, lapangan maupun di rumah.">
    <meta name="google-site-verification" content="+nxGUDJ4QpAZ5l9Bsjdi102tLVC21AIh5d1Nl23908vVuFHs34=" />
    <meta name="robots" content="noindex,nofollow">
    {{-- Style --}}
    @stack('before-style')
    @include('includes.front.style')
    @stack('after-style')
    {{-- End Style --}}


    <!-- =======================================================
  * Template Name: logtifyr - v2.0.0
  * Template URL: https://bootstrapmade.com/logtifyr-free-bootstrap-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>
    {{-- Top Header --}}
    @include('includes.front.topheader')
    {{-- End Top Header --}}
    <!-- ======= Header ======= -->
    @include('includes.front.header')
    <!-- End Header -->

    {{-- Main --}}
    <main id="main">
        {{-- Content --}}
        @yield('content')
        {{-- End Content --}}
    </main>
    <!-- End #main -->

    <!-- ======= Footer ======= -->
    @include('includes.front.footer')
    <!-- End Footer -->

    <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

    {{-- Script --}}
    @include('includes.front.script')
    {{-- End Script --}}


</body>

</html>
