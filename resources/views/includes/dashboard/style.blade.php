<!--Morris Chart CSS -->
<link rel="stylesheet" href="assetsdashboard/plugins/morris/morris.css">
<link href="assetsdashboard/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">

<link href="{{asset('assetsdashboard/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assetsdashboard/css/core.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assetsdashboard/css/components.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assetsdashboard/css/icons.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assetsdashboard/css/pages.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assetsdashboard/css/responsive.css')}}" rel="stylesheet" type="text/css" />

<!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

<script src="{{asset('assetsdashboard/js/modernizr.min.js')}}"></script>
