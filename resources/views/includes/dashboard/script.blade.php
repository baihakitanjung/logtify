<!-- jQuery  -->
<script src="{{asset('assetsdashboard/js/jquery.min.js')}}"></script>
<script src="{{asset('assetsdashboard/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assetsdashboard/js/detect.js')}}"></script>
<script src="{{asset('assetsdashboard/js/fastclick.js')}}"></script>
<script src="{{asset('assetsdashboard/js/jquery.slimscroll.js')}}"></script>
<script src="{{asset('assetsdashboard/js/jquery.blockUI.js')}}"></script>
<script src="{{asset('assetsdashboard/js/waves.js')}}"></script>
<script src="{{asset('assetsdashboard/js/wow.min.js')}}"></script>
<script src="{{asset('assetsdashboard/js/jquery.nicescroll.js')}}"></script>
<script src="{{asset('assetsdashboard/js/jquery.scrollTo.min.js')}}"></script>
<!-- jQuery  -->
<script src="{{asset('assetsdashboard/plugins/moment/moment.js')}}"></script>


<script src="{{asset('assetsdashboard/plugins/morris/morris.min.js')}}"></script>
<script src="{{asset('assetsdashboard/plugins/raphael/raphael-min.js')}}"></script>

<script src="{{asset('assetsdashboard/plugins/bootstrap-sweetalert/sweet-alert.min.js')}}"></script>

<!-- Todojs  -->
<script src="{{asset('assetsdashboard/pages/jquery.todo.js')}}"></script>

<!-- chatjs  -->
<script src="{{asset('assetsdashboard/pages/jquery.chat.js')}}"></script>

<script src="{{asset('assetsdashboard/plugins/peity/jquery.peity.min.js')}}"></script>

<script src="{{asset('assetsdashboard/js/jquery.core.js')}}"></script>
<script src="{{asset('assetsdashboard/js/jquery.app.js')}}"></script>

<script src="{{asset('assetsdashboard/pages/jquery.dashboard_2.js')}}"></script>
