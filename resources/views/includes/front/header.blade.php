<header id="header" class="fixed-top d-flex align-items-center">
    <div class="container d-flex align-items-center">
        <div class="logo mr-auto">
            {{-- <h1 class="text-light"><a href="{{url('/')}}"><span>Logtify</span></a></h1> --}}
            <!-- Uncomment below if you prefer to use an image logo -->
            <a href="{{url('/')}}"><img src="{{asset('assets/img/logo.png')}}" alt="logtify" class="img-fluid"></a>

        </div>

        <nav class="nav-menu d-none d-lg-block">
            <ul>

                <li class="active scrolling"><a href="#header">Home</a></li>
                <li class="scrolling"><a href="#about">About</a></li>
                <li class="scrolling"><a href="#services">Services</a></li>
                {{-- <li><a href="#portfolio">Portfolio</a></li> --}}
                <li class="scrolling"><a href="#team">Team</a></li>
                <li class="scrolling"><a href="#pricing">Pricing</a></li>
                {{-- <li class="drop-down"><a href="">Drop Down</a>
                        <ul>
                            <li><a href="#">Drop Down 1</a></li>
                            <li class="drop-down"><a href="#">Drop Down 2</a>
                                <ul>
                                    <li><a href="#">Deep Drop Down 1</a></li>
                                    <li><a href="#">Deep Drop Down 2</a></li>
                                    <li><a href="#">Deep Drop Down 3</a></li>
                                    <li><a href="#">Deep Drop Down 4</a></li>
                                    <li><a href="#">Deep Drop Down 5</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Drop Down 3</a></li>
                            <li><a href="#">Drop Down 4</a></li>
                            <li><a href="#">Drop Down 5</a></li>
                        </ul>
                    </li> --}}
                <li class="scrolling"><a href="#contact">Contact</a></li>
                <li><a href="{{url('/user/signin')}}" class="sign-in">Sign In</a></li>
                <li class="get-started"><a class="shadow" href="{{url('/user/signup')}}"><strong>Sign Up | FREE
                        </strong></a>
                </li>
            </ul>
        </nav><!-- .nav-menu -->

    </div>
</header>
