<div id="topbar" class="fixed-top">
    <div class="container">
        <ul class="float-right">
            <li><a href="#">Request A Trial</a></li>
            <li><i class='bx bxl-whatsapp mr-1' style=""></i><a href="{{url('/')}}">(021) 296 50555</a></li>
            <li class="sign-in"><a class="btn" href="{{url('/user/signin')}}">Sign In</a></li>
            {{-- <li class="resource">Resources<i class='bx bxs-down-arrow bx-x'></i></li>
            <li>Support</li> --}}
        </ul>
    </div>
</div>
