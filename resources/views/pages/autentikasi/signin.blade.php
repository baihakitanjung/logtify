@extends('layouts.default')

@section('content')
<section id="sign" class="autentikasi">
    <div class="container">
        <div class="autentikasi-title">
            <h2 class="page">Sign In</h2>
        </div>

        <div class="row page">
            <div class="col-md">
                <form action="" method="post" role="form" class="php-email-form">
                    <p><i class="ri-mail-line"></i>Email Address</p>
                    <div class="form-group">
                        <input type="text" name="name" class="form-control" id="name" placeholder="Email Address"
                            data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                        {{-- <div class="validate"></div> --}}
                    </div>
                    <p><i class="ri-key-line"></i>Password</p>
                    <div class="form-group">
                        <input type="password" name="Password" class="form-control" id="password" placeholder="Password"
                            data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                        {{-- <div class="validate"></div> --}}
                    </div>
                    {{-- <div class="mb-3">
                        <div class="loading">Loading</div>
                        <div class="error-message"></div>
                        <div class="sent-message">Your message has been sent. Thank you!</div>
                    </div> --}}
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md mb-2">
                                <span class="captcha-image">{!!Captcha::img();!!}</span>
                                <button class="btn-refresh inline-block "><i class='bx bx-refresh bx-lg'></i></button>
                            </div>
                        </div>
                        <input type="text" name="Password" class="form-control" id="Captcha" placeholder="Captcha"
                            data-rule="minlen:5" data-msg="Please enter at least 5 captcha" />
                    </div>
                    <div class="text-muted policy">
                        <p>Logtify is protected by reCAPTCHA v3 and the Privacy Policy and Terms of Service apply.</p>
                    </div>
                    <div class="text-center "><button class="shadow hover-button" type="submit"><a
                                href="{{url('/user/verification')}}" class="text-white">Sign
                                In</a></button></div>
                    <div class="row mt-2">
                        <div class="col">
                            <div class="form-check text-left">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                <label class="form-check-label" for="defaultCheck1">
                                    Stay Signed In
                                </label>
                            </div>

                        </div>
                        <div class="col mb-1">
                            <p class="text-right">New To Logtify ? <a href="{{url('/user/signup')}}">Sign Up</a></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <p class="text-center">Forgot Password ? <a href="{{url('/user/forgotpassword')}}">Here</a>
                            </p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
