@extends('layouts.default')

@section('content')
<section id="changepassword" class="autentikasi">
    <div class="container">
        <div class="autentikasi-title">
            <h2 class="page">Change Password</h2>
        </div>

        <div class="row page">
            <div class="col-md">
                <form action="" method="post" role="form" class="php-email-form">
                    <p><i class="ri-key-line"></i>Old Password</p>
                    <div class="form-group">
                        <input type="text" name="name" class="form-control" id="name" placeholder="Email Address"
                            data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                        {{-- <div class="validate"></div> --}}
                    </div>
                    <p><i class="ri-key-line"></i>Password</p>
                    <div class="form-group">
                        <input type="password" name="Password" class="form-control" id="password" placeholder="Password"
                            data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                        {{-- <div class="validate"></div> --}}
                    </div>
                    <p><i class="ri-key-line"></i>Confirm Password</p>
                    <div class="form-group">
                        <input type="password" name="Password" class="form-control" id="password" placeholder="Password"
                            data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                        {{-- <div class="validate"></div> --}}
                    </div>
                    {{-- <div class="mb-3">
                        <div class="loading">Loading</div>
                        <div class="error-message"></div>
                        <div class="sent-message">Your message has been sent. Thank you!</div>
                    </div> --}}
                    <div class="text-center "><button class="shadow hover-button" type="submit"><a
                                href="{{url('/user/verification')}}" class="text-white">Sign
                                In</a></button></div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
