@extends('layouts.default')

@section('content')
<section id="forgotpassword" class="autentikasi">
    <div class="container mt-5">
        <div class="autentikasi-title">
            <h2 class="page">Forgot Password</h2>
        </div>

        <div class="row page">
            <div class="col-md">
                <form action="" method="post" role="form" class="php-email-form">
                    <p><i class="ri-mail-line"></i>Email</p>
                    <div class="form-group">
                        <input type="text" name="name" class="form-control" id="name" placeholder="Email Address"
                            data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                        {{-- <div class="validate"></div> --}}
                    </div>
                    {{-- <div class="mb-3">
                        <div class="loading">Loading</div>
                        <div class="error-message"></div>
                        <div class="sent-message">Your message has been sent. Thank you!</div>
                    </div> --}}
                    <div class="text-center mt-4"><button class="shadow hover-button" type="submit"><a
                                href="{{url('/user/verification')}}" class="text-white">Send</a></button></div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
