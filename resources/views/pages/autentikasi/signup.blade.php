@extends('layouts.default')

@section('content')
<section id="sign" class="autentikasi">
    <div class="container">
        <div class="autentikasi-title">
            <h2 class="page">Sign Up</h2>
        </div>

        <div class="row content page">
            <div class="col-md">
                <form action="" method="post" role="form" class="php-email-form">
                    <div class="form-group">
                        <p><i class="ri-mail-line"></i>Email Address</p>
                        <input type="text" name="name" class="form-control" id="name" placeholder="Email Address"
                            data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                        {{-- <div class="validate"></div> --}}
                    </div>
                    <div class="form-group">
                        <p><i class="ri-smartphone-line"></i>Mobile No</p>
                        <input type="text" name="name" class="form-control" id="name" placeholder="Mobile No"
                            data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                        {{-- <div class="validate"></div> --}}
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <p><i class="ri-key-line"></i>Password</p>
                                <input type="password" name="Password" class="form-control" id="password"
                                    placeholder="Password" data-rule="minlen:4"
                                    data-msg="Please enter at least 4 chars" />
                                {{-- <div class="validate"></div> --}}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <p><i class="ri-key-line"></i>Confirm Password</p>
                                <input type="password" name="Password" class="form-control" id="password"
                                    placeholder="Confirm Password" data-rule="minlen:4"
                                    data-msg="Please enter at least 4 chars" />
                                {{-- <div class="validate"></div> --}}
                            </div>
                        </div>
                    </div>
                    {{-- <div class="mb-3">
                        <div class="loading">Loading</div>
                        <div class="error-message"></div>
                        <div class="sent-message">Your message has been sent. Thank you!</div>
                    </div> --}}
                    <div class="text-center mt-3"><button class="shadow hover-button" type="submit"><a
                                class="text-white" href="{{url('/user/profile')}}">Sign
                                Up</a></button>
                    </div>
                    <div class="row mt-2">
                        <div class="col">
                            <p class="text-center">Do You Have Logtify Account ? <a href="{{url('/user/signin')}}">Sign
                                    In</a></p>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</section>
@endsection
