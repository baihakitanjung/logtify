@extends('layouts.default')

@section('content')
<section id="verification" class="autentikasi">
    <div class="container">
        <div class="autentikasi-title">
            <h2 class="page">Entry Your Verification Code</h2>
        </div>

        <div class="row page-picture">
            <div class="col text-center">
                <img src="{{ asset('assets/img/verification.png') }}" alt="" srcset="">
            </div>
        </div>

        <div class="row page">
            <div class="col-md">
                <form action="" method="post" role="form" class="php-email-form">
                    <div class="form-group text-center">
                        {{-- <input type="number" name="name" class="form-control" id="name" placeholder="6 Digits"
                            data-rule="minlen:4" data-msg="Please enter at least 4 chars" /> --}}
                        {{-- <div class="validate"></div> --}}
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <input type="number" name="name" class="form-control" id="name" placeholder="1"
                                    data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                                <input type="number" name="name" class="form-control" id="name" placeholder="2"
                                    data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                                <input type="number" name="name" class="form-control" id="name" placeholder="3"
                                    data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                                <input type="number" name="name" class="form-control" id="name" placeholder="4"
                                    data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                                <input type="number" name="name" class="form-control" id="name" placeholder="5"
                                    data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                                <input type="number" name="name" class="form-control" id="name" placeholder="6"
                                    data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                            </div>
                            {{-- <div class="col-sm-2"><input type="number" name="name" class="form-control" id="name"
                                    placeholder="2" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                            </div>
                            <div class="col-sm-2"><input type="number" name="name" class="form-control" id="name"
                                    placeholder="3" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                            </div>
                            <div class="col-sm-2"><input type="number" name="name" class="form-control" id="name"
                                    placeholder="4" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                            </div>
                            <div class="col-sm-2"><input type="number" name="name" class="form-control" id="name"
                                    placeholder="5" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                            </div>
                            <div class="col-sm-2"><input type="number" name="name" class="form-control" id="name"
                                    placeholder="6" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                            </div> --}}
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <button class="hover-button shadow" type="submit"><a href="{{url('/user/profile')}}"
                                class="text-white">Verification</a>
                        </button>
                    </div>
                    <div class="row mt-2">
                        <div class="col">
                            <p class="text-center" style="color :darkgray;">Your Verification Has Been Sent Email Or Hp
                            </p>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col">
                            <p class="text-center">Resent your code <a href="{{url('/user/signin')}}">Here</a></p>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</section>
@endsection
