@extends('pages.user.home')

@section('user')
<div id="team" class="team">
    <div class="container">
        <div class="row">
            <div class="col-md mt-4 mb-2">
                <div class="float-left">
                    <h3>Please register or upload users here...</h3>
                </div>
                <div class="float-right">
                    <button class="custom-button-blue font-weight-bold" data-toggle="modal" data-target="#modaladdusers"
                        data-whatever="@fat"><i class="ri-add-line"></i></button>
                    <button class="custom-button-blue font-weight-bold" data-toggle="modal"
                        data-target="#modaluploadusers" data-whatever="@fat"><i
                            class="ri-upload-cloud-2-line"></i></button>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md">
                <div class="card p-2">
                    <div class="table-responsive">
                        <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Email</th>
                                    <th>Nama Depan</th>
                                    <th>Nama Belakang</th>
                                    <th>No Hp</th>
                                    <th>Jobtitle</th>
                                    <th>Default Password</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Logtify.com</td>
                                    <td>John</td>
                                    <td>Doe</td>
                                    <td>089776665727</td>
                                    <td>Manager</td>
                                    <td>123456789</td>
                                    <td><button class="btn btn-primary"><i class="ri-pencil-line"></i></button></td>
                                    <td><button class="btn btn-primary"><i class="ri-delete-bin-6-line"></i></button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>Logtify.com</td>
                                    <td>John</td>
                                    <td>Doe</td>
                                    <td>089776665727</td>
                                    <td>Manager</td>
                                    <td>123456789</td>
                                    <td><button class="btn btn-primary"><i class="ri-pencil-line"></i></button></td>
                                    <td><button class="btn btn-primary"><i class="ri-delete-bin-6-line"></i></button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>Logtify.com</td>
                                    <td>John</td>
                                    <td>Doe</td>
                                    <td>089776665727</td>
                                    <td>Manager</td>
                                    <td>123456789</td>
                                    <td><button class="btn btn-primary"><i class="ri-pencil-line"></i></button></td>
                                    <td><button class="btn btn-primary"><i class="ri-delete-bin-6-line"></i></button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>Logtify.com</td>
                                    <td>John</td>
                                    <td>Doe</td>
                                    <td>089776665727</td>
                                    <td>Manager</td>
                                    <td>123456789</td>
                                    <td><button class="btn btn-primary"><i class="ri-pencil-line"></i></button></td>
                                    <td><button class="btn btn-primary"><i class="ri-delete-bin-6-line"></i></button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>Logtify.com</td>
                                    <td>John</td>
                                    <td>Doe</td>
                                    <td>089776665727</td>
                                    <td>Manager</td>
                                    <td>123456789</td>
                                    <td><button class="btn btn-primary"><i class="ri-pencil-line"></i></button></td>
                                    <td><button class="btn btn-primary"><i class="ri-delete-bin-6-line"></i></button>
                                    </td>
                                </tr>

                            </tbody>
                            {{-- <tfoot>
                                    <tr>
                                        <th>Name</th>
                                        <th>Position</th>
                                        <th>Office</th>
                                        <th>Age</th>
                                        <th>Start date</th>
                                        <th>Salary</th>
                                    </tr>
                                </tfoot> --}}
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


{{-- Modal Add--}}
<div class="modal fade" id="modaladdusers" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #054686;color:white;">
                <h5 class="modal-title" id="exampleModalLabel">Add Users
                </h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    {{-- <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Id:</label>
                        <input type="text" class="form-control" id="recipient-name">
                    </div> --}}
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Email:</label>
                        <input type="text" class="form-control" id="recipient-name">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Employee Namee:</label>
                        <input type="text" class="form-control" id="recipient-name">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Position:</label>
                        <input type="text" class="form-control" id="recipient-name">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Date of birth:</label>
                        <input type="date" class="form-control" id="recipient-name">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Mobile Phone:</label>
                        <input type="text" class="form-control" id="recipient-name">
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">Status</label>
                        <select class="form-control" id="exampleFormControlSelect1">
                            <option>Active</option>
                            <option>No Active</option>
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer text-center">
                {{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> --}}
                <button type="button text-center" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </div>
</div>

{{-- Modal Upload --}}
<div class="modal fade" id="modaluploadusers" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #054686;color:white;">
                <h5 class="modal-title" id="exampleModalLabel">Add Users</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="container-fluid">
                        {{-- <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Id:</label>
                        <input type="text" class="form-control" id="recipient-name">
                    </div> --}}
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <button type="button" class="btn btn-primary w-100">Browser<i
                                        class="ri-folder-line ml-2"></i>
                                </button>
                            </div>
                        </div>
                        <div class="row text-center">
                            <div class="col-md-12 mt-2">
                                <h3>Sumarry</h3>
                            </div>
                            <div class="col-md-12">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">File Name</th>
                                            <th scope="col">Total Row</th>
                                            <th scope="col">Count of User</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>User.xlxs</td>
                                            <td>100</td>
                                            <td>100</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer text-center">
                {{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> --}}
                <button type="button" class="btn btn-primary">Upload</button>
            </div>
        </div>
    </div>
</div>
@endsection
