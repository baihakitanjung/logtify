@extends('layouts.default')

@section('content')
<section id="homeuser" class="homeuser">
  <div class="container">
    <div class="sidebar-rens mt-5">
      <a class="btn btn-primary btn-block" data-toggle="collapse" href="#collapseExample" role="button"
        aria-expanded="false" aria-controls="collapseExample">
        <span class="float-left">Menu</span>
        <i class='bx bx-down-arrow float-right mt-1'></i>
        <div class="clearfix"></div>
      </a>
      <div class="collapse" id="collapseExample">
        <div class="col-md-3 sidebar">
          <div class="personal">
            <strong>
              <p>PERSONAL</p>
            </strong>
            <div class="list-group list-group-flush">
              <a href="{{url('/user/profile')}}" class="list-group-item list-group-item-action active">
                Profile
              </a>
              <a href="{{url('/user/time')}}" class="list-group-item list-group-item-action">Time
                Attendance</a>
              <a href="{{url('/user/employee')}}" class="list-group-item list-group-item-action">Employee Self Services</a>
              <a href="{{url('/user/activity')}}" class="list-group-item list-group-item-action">Tracking
                Activity</a>
              <a href="{{url('/user/reward')}}" class="list-group-item list-group-item-action">Reward and
                Punishment</a>
              <a href="{{url('/user/history')}}" class="list-group-item list-group-item-action">History</a>
              <a href="{{url('/user/history')}}" class="list-group-item list-group-item-action">MyTeam</a>
              {{-- <a href="#" class="list-group-item list-group-item-action disabled" tabindex="-1"
                                                aria-disabled="true">Vestibulum at
                                                eros</a> --}}
            </div>
          </div>
          <div class="admin mt-3">
            <strong>
              <p>ADMIN</p>
            </strong>
            <div class="list-group">
              <div class="accordion" id="accordionExample">
                <div class="card">
                  <div class="card-header" id="headingOne">
                    <h2 class="mb-0">
                      <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse"
                        data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseOne">
                        User Management<i class='bx bx-down-arrow bx-xs ml-2'></i>
                      </button>
                    </h2>
                  </div>

                  <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                      <div class="list-group list-group-flush">
                        <a href="{{url('/user/users')}}" class="list-group-item list-group-item-action">
                          Users Management
                        </a>
                        <a href="{{url('/user/role')}}" class="list-group-item list-group-item-action">Role
                          User</a>
                        <a href="{{url('/user/role')}}" class="list-group-item list-group-item-action">Role
                          Management</a>
                        {{-- <a href="#" class="list-group-item list-group-item-action disabled" tabindex="-1"
                                                                                        aria-disabled="true">Vestibulum at
                                                                                        eros</a> --}}
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header" id="headingTwo">
                    <h2 class="mb-0">
                      <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse"
                        data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseOne">
                        News Management<i class='bx bx-down-arrow bx-xs ml-2'></i>
                      </button>
                    </h2>
                  </div>

                  <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                    <div class="card-body">
                      <div class="list-group list-group-flush">
                        <a href="{{url('/user/users')}}" class="list-group-item list-group-item-action">
                          Users
                        </a>
                        <a href="{{url('/user/role')}}" class="list-group-item list-group-item-action">Role
                          Management</a>
                        <a href="{{url('/user/team')}}" class="list-group-item list-group-item-action">Team</a>
                        {{-- <a href="#" class="list-group-item list-group-item-action disabled" tabindex="-1"
                                                                                        aria-disabled="true">Vestibulum at
                                                                                        eros</a> --}}
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header" id="headingThree">
                    <h2 class="mb-0">
                      <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse"
                        data-target="#collapseThree" aria-expanded="false" aria-controls="collapsethree">
                        Announcement Management<i class='bx bx-down-arrow bx-xs ml-2'></i>
                      </button>
                    </h2>
                  </div>

                  <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                    data-parent="#accordionExample">
                    <div class="card-body">
                      <div class="list-group list-group-flush">
                        <a href="{{url('/user/users')}}" class="list-group-item list-group-item-action">
                          Users
                        </a>
                        <a href="{{url('/user/role')}}" class="list-group-item list-group-item-action">Role
                          Management</a>
                        <a href="{{url('/user/team')}}" class="list-group-item list-group-item-action">Team</a>
                        {{-- <a href="#" class="list-group-item list-group-item-action disabled" tabindex="-1"
                                                                                                                    aria-disabled="true">Vestibulum at
                                                                                                                    eros</a> --}}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {{-- <a href="#" class="list-group-item list-group-item-action">Dapibus ac facilisis in</a>
                                            <a href="#" class="list-group-item list-group-item-action">Morbi leo risus</a>
                                            <a href="#" class="list-group-item list-group-item-action">Porta ac consectetur ac</a>
                                            <a href="#" class="list-group-item list-group-item-action disabled" tabindex="-1"
                                                aria-disabled="true">Vestibulum at
                                                eros</a> --}}
            </div>
          </div>
          <div class="row">
            <div class="col tambahan-sidebar">
              <div class="row mt-3">
                <a href="">Attend Live Training</a>
              </div>
              <div class="row mt-3">
                <a href="">Video Tutorial</a>

              </div>
              <div class="row mt-3">
                <a href="">Knowledge Base</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row mt-5">
      <div class="col-md-3 sidebar sidebar-hidden">
        <div class="personal">
          <strong>
            <p>PERSONAL</p>
          </strong>
          <div class="list-group list-group-flush">
            <a href="{{url('/user/profile')}}" class="list-group-item list-group-item-action active">
              Profile
            </a>
            <a href="{{url('/user/time')}}" class="list-group-item list-group-item-action">Time
              Attendance</a>
            <a href="{{url('/user/employee')}}" class="list-group-item list-group-item-action">Employee Self Services</a>
            <a href="{{url('/user/activity')}}" class="list-group-item list-group-item-action">Tracking
              Activity</a>
            <a href="{{url('/user/reward')}}" class="list-group-item list-group-item-action">Reward and
              Punishment</a>
            <a href="{{url('/user/history')}}" class="list-group-item list-group-item-action">History</a>
            <a href="{{url('/user/history')}}" class="list-group-item list-group-item-action">MyTeam</a>
            {{-- <a href="#" class="list-group-item list-group-item-action disabled" tabindex="-1"
                            aria-disabled="true">Vestibulum at
                            eros</a> --}}
          </div>
        </div>
        <div class="admin mt-3">
          <strong>
            <p>ADMIN</p>
          </strong>
          <div class="list-group">
            <div class="accordion" id="accordionExample">
              <div class="card">
                <div class="card-header" id="headingOne">
                  <h2 class="mb-0">
                    <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse"
                      data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseOne">
                      User Management<i class='bx bx-down-arrow bx-xs ml-2'></i>
                    </button>
                  </h2>
                </div>

                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                  <div class="card-body">
                    <div class="list-group list-group-flush">
                      <a href="{{url('/user/users')}}" class="list-group-item list-group-item-action">
                        Users Management
                      </a>
                      <a href="{{url('/user/role')}}" class="list-group-item list-group-item-action">Role
                        User</a>
                      <a href="{{url('/user/role')}}" class="list-group-item list-group-item-action">Role
                        Management</a>
                      {{-- <a href="#" class="list-group-item list-group-item-action disabled" tabindex="-1"
                                                                    aria-disabled="true">Vestibulum at
                                                                    eros</a> --}}
                    </div>
                  </div>
                </div>
              </div>
              <div class="card">
                <div class="card-header" id="headingTwo">
                  <h2 class="mb-0">
                    <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse"
                      data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseOne">
                      News Management<i class='bx bx-down-arrow bx-xs ml-2'></i>
                    </button>
                  </h2>
                </div>

                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                  <div class="card-body">
                    <div class="list-group list-group-flush">
                      <a href="{{url('/user/users')}}" class="list-group-item list-group-item-action">
                        Users
                      </a>
                      <a href="{{url('/user/role')}}" class="list-group-item list-group-item-action">Role
                        Management</a>
                      <a href="{{url('/user/team')}}" class="list-group-item list-group-item-action">Team</a>
                      {{-- <a href="#" class="list-group-item list-group-item-action disabled" tabindex="-1"
                                                                    aria-disabled="true">Vestibulum at
                                                                    eros</a> --}}
                    </div>
                  </div>
                </div>
              </div>
              <div class="card">
                <div class="card-header" id="headingThree">
                  <h2 class="mb-0">
                    <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse"
                      data-target="#collapseThree" aria-expanded="false" aria-controls="collapsethree">
                      Announcement Management<i class='bx bx-down-arrow bx-xs ml-2'></i>
                    </button>
                  </h2>
                </div>

                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                  <div class="card-body">
                    <div class="list-group list-group-flush">
                      <a href="{{url('/user/users')}}" class="list-group-item list-group-item-action">
                        Users
                      </a>
                      <a href="{{url('/user/role')}}" class="list-group-item list-group-item-action">Role
                        Management</a>
                      <a href="{{url('/user/team')}}" class="list-group-item list-group-item-action">Team</a>
                      {{-- <a href="#" class="list-group-item list-group-item-action disabled" tabindex="-1"
                                                                                                aria-disabled="true">Vestibulum at
                                                                                                eros</a> --}}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {{-- <a href="#" class="list-group-item list-group-item-action">Dapibus ac facilisis in</a>
                        <a href="#" class="list-group-item list-group-item-action">Morbi leo risus</a>
                        <a href="#" class="list-group-item list-group-item-action">Porta ac consectetur ac</a>
                        <a href="#" class="list-group-item list-group-item-action disabled" tabindex="-1"
                            aria-disabled="true">Vestibulum at
                            eros</a> --}}
          </div>
        </div>
        <div class="row">
          <div class="col tambahan-sidebar">
            <div class="row mt-3">
              <a href="">Attend Live Training</a>
            </div>
            <div class="row mt-3">
              <a href="">Video Tutorial</a>

            </div>
            <div class="row mt-3">
              <a href="">Knowledge Base</a>
            </div>
          </div>
        </div>

      </div>
      <div class="col-md-9 mt-2">
        @yield('user')
      </div>
    </div>
  </div>
</section>
@endsection

