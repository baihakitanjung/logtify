@extends('pages.user.home')

@section('user')
<div id="profile" class="profile">
    <div class="container">

        <div class="row">
            <div class="col-md">
                <img src="https://massive.mpcthemes.net/wp-content/uploads/2016/02/avatar03.jpg" alt="avatar"
                    class="rounded-circle">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    {{-- <div class="gambar-card">
                        <img src="{{asset('assets/img/wave.svg')}}" alt="">
                </div> --}}
                <div class="container">
                    <div class="row ml-5 mr-5">
                        <div class="col-md-6">
                            <div class="namalengkap">
                                <p>Nama Lengkap</p>
                                <h3>John Doe</h3>
                            </div>
                            <div class="namadepan">
                                <p>Nama Depan</p>
                                <h3>John</h3>
                            </div>
                            <div class="namabelakang">
                                <p>Nama Belakang</p>
                                <h3>Doe</h3>
                            </div>
                            <div class="tanggallahir">
                                <p>Tanggal Lahir</p>
                                <h3>29 Februari 2020</h3>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="bussinesscale">
                                <p>Bussines Scale</p>
                                <h3>1 - 100</h3>
                            </div>
                            <div class="payment">
                                <p>Payment Method</p>
                                <h3>BCA</h3>
                            </div>
                            <div class="tomboledit mb-5">
                                <button>EDIT</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
