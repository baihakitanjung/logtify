@extends('layouts.default')

@section('content')
<section id="moreabout" class="autentikasi">
    <div class="container">
        <div class="picture">
            <div class="row">
                <div class="col-md">
                    <img src="{{asset('assets/img/logo.png')}}" alt="" class="img-fluid">
                </div>
            </div>
        </div>
        <div class="autentikasi-title">
            <h2 class="page">Apa itu Logtify ?</h2>
        </div>

        <div class="row page">
            <div class="col-md text-center">
                Berangkat dari budaya WFH karena pandemi Corona yang terjadi di Indonesia pada saat ini,
                PT Solusi Integrasi Pratama berinovasi dengan mengeluarkan sebuah aplikasi HR berbasis cloud yang
                bertujuan membantu
                perusahaan untuk memonitor proses absensi, pekerjaan masing-masing karyawan serta memberikan
                pembelejaran online untuk
                meningkatkan produktivitas karyawan anda.
            </div>
        </div>
    </div>
</section>
@endsection
