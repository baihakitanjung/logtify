@extends('layouts.default')

@section('content')

{{-- Hero Section --}}
@include('pages.front.hero')
{{-- End Hero Section --}}

<!-- ======= Clients Section ======= -->
{{-- @include('pages.front.clients') --}}
<!-- End Clients Section -->

<!-- ======= About Us Section ======= -->
@include('pages.front.about')
<!-- End About Us Section -->

<!-- ======= Counts Section ======= -->
@include('pages.front.counts')
<!-- End Counts Section -->

<!-- ======= Services Section ======= -->
@include('pages.front.services')
<!-- End Services Section -->

<!-- ======= More Services Section ======= -->
@include('pages.front.more-services')
<!-- End More Services Section -->

<!-- ======= Features Section ======= -->
{{-- @include('pages.front.features') --}}
<!-- End Features Section -->

<!-- ======= Testimonials Section ======= -->
{{-- @include('pages.front.testimonials') --}}
<!-- End Testimonials Section -->

<!-- ======= Portfolio Section ======= -->
{{-- @include('pages.front.portfolio') --}}
<!-- End Portfolio Section -->

<!-- ======= Team Section ======= -->
{{-- @include('pages.front.team') --}}
<!-- End Team Section -->

<!-- ======= Pricing Section ======= -->
@include('pages.front.pricing')
<!-- End Pricing Section -->

<!-- ======= F.A.Q Section ======= -->
{{-- @include('pages.front.faq') --}}
<!-- End F.A.Q Section -->

<!-- ======= Contact Section ======= -->
@include('pages.front.contact')
<!-- End Contact Section -->



@endsection
