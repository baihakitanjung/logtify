<section id="more-services" class="more-services">
    <div class="container">

        <div class="row">
            <div class="col-md-6 d-flex align-items-stretch">
                <div class="card" style='background-image: url("assets/img/more-services/notifications.png"); background-repeat:no-repeat;
    background-position:70% 15%;' data-aos="fade-up" data-aos-delay="100">
                    <div class="card-body">
                        <h5 class="card-title"><a href="">Mobile Attendance</a></h5>
                        <p class="card-text">Selfie Validation, GPS Validation, Multiple Clock in/out, Offline Mode,
                            Fingerprint (optional), Permission (Optional)</p>
                        <div class="read-more"><a href="{{ url('/page/moreservices') }}"><i
                                    class="icofont-arrow-right"></i> Read More</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 d-flex align-items-stretch mt-4 mt-md-0">
                <div class="card" style='background-image: url("assets/img/more-services/note_taking.png"); background-repeat:no-repeat;
    background-position:70% 15%;' data-aos="fade-up" data-aos-delay="200">
                    <div class="card-body">
                        <h5 class="card-title"><a href="">Tracking Activity</a></h5>
                        <p class="card-text">Upload Plan Activity, Tracking Proses Activity, Upload Result Activity</p>
                        <div class="read-more"><a href="{{ url('/page/moreservices') }}"><i
                                    class="icofont-arrow-right"></i> Read More</a>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-6 d-flex align-items-stretch mt-4">
                <div class="card" style='background-image: url("assets/img/more-services/profiling_monochromatic.png"); background-repeat:no-repeat;
    background-position:70% 15%;' data-aos="fade-up" data-aos-delay="100">
                    <div class="card-body">
                        <h5 class="card-title"><a href="">Evaluation Tools</a></h5>
                        <p class="card-text">Approval Plan Activity oleh Atasan, Monitoring Progress Activity Karyawan, Evaluasi Result Pekerjaan Karyawan oleh Atasan</p>
                        <div class="read-more"><a href="{{ url('/page/moreservices') }}"><i
                                    class="icofont-arrow-right"></i> Read More</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 d-flex align-items-stretch mt-4">
                <div class="card" style='background-image: url("assets/img/more-services/note_taking_monochromatic.png"); background-repeat:no-repeat;
    background-position:70% 15%;' data-aos="fade-up" data-aos-delay="200">
                    <div class="card-body">
                        <h5 class="card-title"><a href="">Learning Management System</a></h5>
                        <p class="card-text">Attend Live Training, Tracking Proses Activity, Upload Result Activity</p>
                        <div class="read-more"><a href="{{ url('/page/moreservices') }}"><i
                                    class="icofont-arrow-right"></i> Read More</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 d-flex align-items-stretch mt-4">
                <div class="card" style='background-image: url("assets/img/more-services/achievement_.png");background-repeat:no-repeat;
    background-position:70% 15%;' data-aos="fade-up" data-aos-delay="200">
                    <div class="card-body">
                        <h5 class="card-title"><a href="">Reward & Punishment Management</a></h5>
                        <p class="card-text">Reward (Reddem Point), Punishment (Sanksi & Apologize)</p>
                        <div class="read-more"><a href="{{ url('/page/moreservices') }}"><i
                                    class="icofont-arrow-right"></i> Read More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
