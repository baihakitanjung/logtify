<section id="contact" class="contact">
    <div class="container">

        <div class="section-title" data-aos="fade-up">
            <h2>Contact Us</h2>
        </div>

        <div class="row">

            <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="100">
                <div class="contact-about">
                    <a href="{{url('/')}}"><img src="{{asset('assets/img/logo.png')}}" alt="logtify"
                            class="img-fluid"></a>
                    <p>Solusi aplikasi produktivitas bisnis yang tepat bagi perusahaan anda dari beberapa kendala yang
                        ada saat ini baik untuk
                        karyawan yang bekerja di kantor, lapangan maupun di rumah.</p>
                    <div class="social-links">
                        <a href="#" class="twitter"><i class="icofont-twitter"></i></a>
                        <a href="#" class="facebook"><i class="icofont-facebook"></i></a>
                        <a href="https://www.instagram.com/sitama.indonesia/" class="instagram"><i
                                class="icofont-instagram"></i></a>
                        <a href="#" class="linkedin"><i class="icofont-linkedin"></i></a>
                    </div>
                    <div data-aos="fade-up" data-aos-delay="100">
                        <iframe
                            src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15863.060516818086!2d106.6661884!3d-6.2945668!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xa30f945ff433686!2sBFI%20Tower!5e0!3m2!1sid!2sid!4v1592188903326!5m2!1sid!2sid"
                            frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false"
                            tabindex="0"></iframe>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 mt-4 mt-md-0" data-aos="fade-up" data-aos-delay="200">
                <div class="info">
                    <div>
                        <i class="ri-map-pin-line"></i>
                        <p>BFI Tower Lantai 8
                            Sunburst CBD Lot 1.2
                            Jl. Kapten Soebijanto Djojohadikusumo<br>Bsd City,
                            Tangerang Selatan - 15322</p>
                    </div>

                    <div>
                        <i class="ri-mail-send-line"></i>
                        <p>contact@logtify.co.id</p>
                    </div>

                    <div>
                        <i class="ri-phone-line"></i>
                        <p>(021) 296 50555</p>
                    </div>


                </div>
            </div>

            <div class="col-lg-5 col-md-12" data-aos="fade-up" data-aos-delay="300">
                <form action="forms/contact.php" method="post" role="form" class="php-email-form">
                    <div class="form-group">
                        <input type="text" name="name" class="form-control" id="name" placeholder="Your Name"
                            data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                        <div class="validate"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" id="email"
                                    placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                                <div class="validate"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="number" class="form-control" name="hp" id="hp" placeholder="Your Mobile No"
                                    data-rule="email" data-msg="Please enter a valid number" />
                                <div class="validate"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject"
                            data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                        <div class="validate"></div>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" name="message" rows="5" data-rule="required"
                            data-msg="Please write something for us" placeholder="Message"></textarea>
                        <div class="validate"></div>
                    </div>
                    <div class="form-group">
                        <div class="g-recaptcha" data-sitekey="6LdjtKUZAAAAABKCTaPdvp_QFB6bCX6Uk8YUCTnb"></div>
                    </div>
                    <div class="mb-3">
                        <div class="loading">Loading</div>
                        <div class="error-message"></div>
                        <div class="sent-message">Your message has been sent. Thank you!</div>
                    </div>
                    <div class="text-center"><button type="submit">Send Message</button></div>
                </form>
            </div>

        </div>

    </div>
</section>
