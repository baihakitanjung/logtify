<section id="counts" class="counts">
    <div class="container">

        <div class="row">
            <div class="image col-xl-5 d-flex align-items-stretch justify-content-center justify-content-xl-start"
                data-aos="fade-right" data-aos-delay="150">
                <img src="assets/img/counts-img.svg" alt="" class="img-fluid">
            </div>

            <div class="col-xl-7 d-flex align-items-stretch pt-4 pt-xl-0" data-aos="fade-left" data-aos-delay="300">
                <div class="content d-flex flex-column justify-content-center">
                    <div class="row">
                        <div class="col-md-6 d-md-flex align-items-md-stretch">
                            <div class="count-box">
                                <i class="icofont-focus"></i>
                                {{-- <span data-toggle="counter-up">65</span> --}}
                                <p><strong>Akurat</strong> Logtify tidak hanya mencatat nama karyawan dan waktu
                                    kehadiran, tetapi dengan selfie validation, GPS validation dan
                                    fingerprint.</p>
                            </div>
                        </div>

                        <div class="col-md-6 d-md-flex align-items-md-stretch">
                            <div class="count-box">
                                <i class="icofont-smart-phone"></i>
                                {{-- <span data-toggle="counter-up">85</span> --}}
                                <p><strong>Fleksibel</strong> Logtify dapat di akses secara mobile melalui handphone dan
                                    website melalui PC/laptop. Logtify membuat kita dapat bekerja
                                    dimana saja</p>
                            </div>
                        </div>

                        <div class="col-md-6 d-md-flex align-items-md-stretch">
                            <div class="count-box">
                                <i class="icofont-users-alt-3"></i>
                                {{-- <span data-toggle="counter-up">12</span> --}}
                                <p><strong>User Friendly</strong> Logtify dapat digunakan dengan saat mudah oleh
                                    karyawan, atasan, dan HRD bahkan karyawan baru sekalipun</p>
                            </div>
                        </div>

                        <div class="col-md-6 d-md-flex align-items-md-stretch">
                            <div class="count-box">
                                <i class="icofont-money-bag"></i>
                                {{-- <span data-toggle="counter-up">15</span> --}}
                                <p><strong>Hemat</strong> Logtify membuat perusahaan tidak perlu membeli mesin absensi
                                    fingerprint, dan membuat biaya absensi menjadi variable
                                    cost.</p>
                            </div>
                        </div>
                    </div>
                </div><!-- End .content-->
            </div>
        </div>

    </div>
</section>
