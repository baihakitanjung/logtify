<section id="features" class="features">
    <div class="container">

        <div class="section-title" data-aos="fade-up">
            <h2>Features</h2>
            <p>Fitur - Fitur yang kami sediakan : </p>
        </div>

        <div class="row" data-aos="fade-up" data-aos-delay="300">
            <div class="col-lg-3 col-md-4">
                <div class="icon-box">
                    <i class="ri-volume-down-line" style="color: #ffbb2c;"></i>
                    <h3><a href="">Announchment</a></h3>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 mt-4 mt-md-0">
                <div class="icon-box">
                    <i class="ri-notification-line" style="color: #5578ff;"></i>
                    <h3><a href="">Notification</a></h3>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 mt-4 mt-md-0">
                <div class="icon-box">
                    <i class="ri-article-line" style="color: #e80368;"></i>
                    <h3><a href="">News Update</a></h3>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 mt-4 mt-lg-0">
                <div class="icon-box">
                    <i class="ri-checkbox-line" style="color: #e361ff;"></i>
                    <h3><a href="">Mobile Attendance</a></h3>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 mt-4">
                <div class="icon-box">
                    <i class="ri-emotion-happy-line" style="color: #47aeff;"></i>
                    <h3><a href="">Selfie Validation</a></h3>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 mt-4">
                <div class="icon-box">
                    <i class="ri-checkbox-multiple-line" style="color: #ffa76e;"></i>
                    <h3><a href="">Multiple Clock in/out</a></h3>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 mt-4">
                <div class="icon-box">
                    <i class="ri-map-pin-line" style="color: #11dbcf;"></i>
                    <h3><a href="">GPS Validation / Geotagging</a></h3>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 mt-4">
                <div class="icon-box">
                    <i class="ri-history-line" style="color: #4233ff;"></i>
                    <h3><a href="">History</a></h3>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 mt-4">
                <div class="icon-box">
                    <i class="ri-logout-box-line" style="color: #b2904f;"></i>
                    <h3><a href="">Permission & Leave (optional)</a></h3>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 mt-4">
                <div class="icon-box">
                    <i class="ri-map-2-line" style="color: #b20969;"></i>
                    <h3><a href="">Tracking Activity/Project (optional)</a></h3>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 mt-4">
                <div class="icon-box">
                    <i class="ri-computer-line" style="color: #ff5828;"></i>
                    <h3><a href="">Monitoring Team</a></h3>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 mt-4">
                <div class="icon-box">
                    <i class="ri-gift-line" style="color: #29cc61;"></i>
                    <h3><a href="">Reward & Punishment (optional)</a></h3>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 mt-4">
                <div class="icon-box">
                    <i class="ri-book-open-line" style="color: #e71a4e;"></i>
                    <h3><a href="">Learning Management System (optional)</a></h3>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 mt-4">
                <div class="icon-box">
                    <i class="ri-fingerprint-line" style="color: #723809;"></i>
                    <h3><a href="">Fingerprint</a></h3>
                </div>
            </div>
        </div>

    </div>
</section>
