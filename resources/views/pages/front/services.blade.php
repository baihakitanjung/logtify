<section id="services" class="services">
    <div class="container">

        <div class="section-title" data-aos="fade-up">
            <h2>Services</h2>
            <p>Services yang kami sediakan : </p>
        </div>

        {{-- <div class="row">
            <div class="col-md-6 col-lg d-flex align-items-stretch mb-5 mb-lg-0">
                <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                    <div class="icon"><i class="bx bx-time"></i></div>
                    <h4 class="title"><a href="">Mobile Attendance</a></h4>
                    <p class="description">Kamu bisa absen dimana saja dengan fitur Selfie Detection, GPS Validation &
                        Finger Print lewat mobile app logtify. Aplikasi ini juga bisa izin cuti atau izin lainnya loh.
                    </p>
                </div>
            </div>

            <div class="col-md-6 col-lg d-flex align-items-stretch mb-5 mb-lg-0">
                <div class="icon-box" data-aos="fade-up" data-aos-delay="200">
                    <div class="icon"><i class="bx bx-user"></i></div>
                    <h4 class="title"><a href="">Tracking Activity</a></h4>
                    <p class="description">Kamu bisa upload plan pekerjaan atau project kamu lewat logtify. Atasan kamu
                        bisa monitoring dan evaluasi pekerjaan kamu juga dengan aplikasi yang sama.</p>
                </div>
            </div>

            <div class="col-md-6 col-lg d-flex align-items-stretch mb-5 mb-lg-0">
                <div class="icon-box" data-aos="fade-up" data-aos-delay="300">
                    <div class="icon"><i class="bx bx-tachometer"></i></div>
                    <h4 class="title"><a href="">Evaluation Tools</a></h4>
                    <p class="description">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
                        officia</p>
                </div>
            </div>

            <div class="col-md-6 col-lg d-flex align-items-stretch mb-5 mb-lg-0">
                <div class="icon-box" data-aos="fade-up" data-aos-delay="400">
                    <div class="icon"><i class="bx bx-book"></i></div>
                    <h4 class="title"><a href="">Learning Management System</a></h4>
                    <p class="description">Ketika evaluasi kamu belum memuaskan, jangan khawatir. kamu bisa belajar
                        lewat seminar Web oleh Sitama Learning Centre dengan Free selamanya</p>
                </div>
            </div>
            <div class="col-md-6 col-lg d-flex align-items-stretch mb-5 mb-lg-0">
                <div class="icon-box" data-aos="fade-up" data-aos-delay="400">
                    <div class="icon"><i class="bx bx-gift"></i></div>
                    <h4 class="title"><a href="">Reward & Punishment Management</a></h4>
                    <p class="description">At vero eos et accusamus et iusto odio dignissimos ducimus qui
                        blanditiis</p>
                </div>
            </div>
        </div> --}}

    </div>
</section>
