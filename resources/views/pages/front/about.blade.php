<section id="about" class="about">
    <div class="container">

        <div class="section-title" data-aos="fade-up">
            <h2>About Us</h2>
        </div>

        <div class="row content">
            <div class="col-lg-6 video-box" data-aos="fade-up" data-aos-delay="150">

                <a href="https://www.youtube.com/watch?v=NoZ8S3UgDh0" class="venobox play-btn mb-4" data-vbtype="video"
                    data-autoplay="true"></a>
            </div>
            <div class="col-lg-6 pt-4 pt-lg-0" data-aos="fade-up" data-aos-delay="300">
                <p>
                    <strong>Logtify</strong> merupakan sebuah aplikasi HR berbasis cloud yang di kembangkan oleh PT.
                    Solusi Integrasi Pratama untuk mempermudah
                    karyawan dalam melakukan absensi, memantau aktivitas karyawan, dan sebagai media belajar bagi
                    karyawan yang dikemas
                    secara online untuk meningkatkan produktivitas karyawan dan meningkatkan profit perusahaan.Logtify
                    adalah solusi
                    aplikasi HR yang tepat bagi perusahaan anda dari beberapa kendala yang ada saat ini baik untuk
                    karyawan yang bekerja di
                    kantor, lapangan maupun di rumah.
                </p>
                <ul>
                    <li><i class="ri-check-double-line"></i> Mobilitas Time Attendance</li>
                    <li><i class="ri-check-double-line"></i> Activity Tracking Management with Objective Key Result</li>
                    <li><i class="ri-check-double-line"></i> Reward Management</li>
                </ul>
                <a href="{{ url('/page/moreabout') }}" class="btn-learn-more">Learn More</a>
            </div>
        </div>

    </div>
</section>
