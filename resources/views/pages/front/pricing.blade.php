<section id="pricing" class="pricing">
    <div class="container">

        <div class="section-title">
            <h2>Pricing</h2>
            <p>Pricing yang kami sediakan :</p>
        </div>

        <div class="row">

            <div class="col-lg-4 col-md-6">
                <div class="box" data-aos="zoom-in-right" data-aos-delay="200">
                    <h3>Basic</h3>
                    <h4><sup>Rp</sup>4.000<span><small> user</small> / month</span></h4>
                    <ul>
                        <li><i class="ri-check-line" style="color: #17e465;"></i>Notification</li>
                        <li><i class="ri-check-line" style="color: #17e465;"></i>News Update</li>
                        <li><i class="ri-check-line" style="color: #17e465;"></i>Mobile Attendance</li>
                        <li><i class="ri-check-line" style="color: #17e465;"></i>Selfie Validation</li>
                        <li><i class="ri-check-line" style="color: #17e465;"></i>GPS Validation</li>
                        <li><i class="ri-check-line" style="color: #17e465;"></i>Multiple ClockIn/Out</li>
                        <li><i class="ri-check-line" style="color: #17e465;"></i>Offline Mode</li>
                        <li><i class="ri-check-line" style="color: #17e465;"></i>Fingerprint (optional)</li>
                        <li><i class="ri-check-line" style="color: #17e465;"></i>Permission (optional)</li>
                        <li><i class="ri-check-line" style="color: #17e465;"></i>History</li>
                        <li><i class="na" style="color: #17e465;"></i>My Team/Monitoring</li>
                        <li><i class="na" style="color: #17e465;"></i>Tracking Activity/Project</li>
                        <li><i class="na" style="color: #17e465;"></i>Evaluation Tools</li>
                        <li><i class="na" style="color: #17e465;"></i>Learning Management</li>
                        <li><i class="na" style="color: #17e465;"></i>Reward & Punishment</li>
                    </ul>
                    <div class="btn-wrap">
                        <a href="#" class="btn-buy">Buy Now</a>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 mt-4 mt-md-0">
                <div class="box recommended" data-aos="zoom-in" data-aos-delay="100">
                    <h3>Productive</h3>
                    <h4><sup>Rp</sup>10.000<span><small> user</small> / month</span></h4>
                    <ul>
                        <li><i class="ri-check-line" style="color: #17e465;"></i>Notification</li>
                        <li><i class="ri-check-line" style="color: #17e465;"></i>News Update</li>
                        <li><i class="ri-check-line" style="color: #17e465;"></i>Mobile Attendance</li>
                        <li><i class="ri-check-line" style="color: #17e465;"></i>Selfie Validation</li>
                        <li><i class="ri-check-line" style="color: #17e465;"></i>GPS Validation</li>
                        <li><i class="ri-check-line" style="color: #17e465;"></i>Multiple ClockIn/Out</li>
                        <li><i class="ri-check-line" style="color: #17e465;"></i>Offline Mode</li>
                        <li><i class="ri-check-line" style="color: #17e465;"></i>Fingerprint (optional)</li>
                        <li><i class="ri-check-line" style="color: #17e465;"></i>Permission (optional)</li>
                        <li><i class="ri-check-line" style="color: #17e465;"></i>History</li>
                        <li><i class="ri-check-line" style="color: #17e465;"></i>My Team/Monitoring</li>
                        <li><i class="ri-check-line" style="color: #17e465;"></i>Tracking Activity/Project</li>
                        <li><i class="ri-check-line" style="color: #17e465;"></i>Evaluation Tools</li>
                        <li><i class="na" style="color: #17e465;"></i>Learning Management</li>
                        <li><i class="na" style="color: #17e465;"></i>Reward & Punishment</li>
                    </ul>
                    <div class="btn-wrap">
                        <a href="#" class="btn-buy">Buy Now</a>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 mt-4 mt-lg-0">
                <div class="box" data-aos="zoom-in-left" data-aos-delay="200">
                    <h3>Professional</h3>
                    <h4><sup>Rp</sup>15.000<span><small> user</small> / month</span></h4>
                    <ul>
                        <li><i class="ri-check-line" style="color: #17e465;"></i>Notification</li>
                        <li><i class="ri-check-line" style="color: #17e465;"></i>News Update</li>
                        <li><i class="ri-check-line" style="color: #17e465;"></i>Mobile Attendance</li>
                        <li><i class="ri-check-line" style="color: #17e465;"></i>Selfie Validation</li>
                        <li><i class="ri-check-line" style="color: #17e465;"></i>GPS Validation</li>
                        <li><i class="ri-check-line" style="color: #17e465;"></i>Multiple ClockIn/Out</li>
                        <li><i class="ri-check-line" style="color: #17e465;"></i>Offline Mode</li>
                        <li><i class="ri-check-line" style="color: #17e465;"></i>Fingerprint (optional)</li>
                        <li><i class="ri-check-line" style="color: #17e465;"></i>Permission (optional)</li>
                        <li><i class="ri-check-line" style="color: #17e465;"></i>History</li>
                        <li><i class="ri-check-line" style="color: #17e465;"></i>My Team/Monitoring</li>
                        <li><i class="ri-check-line" style="color: #17e465;"></i>Tracking Activity/Project</li>
                        <li><i class="ri-check-line" style="color: #17e465;"></i>Evaluation Tools</li>
                        <li><i class="ri-check-line" style="color: #17e465;"></i>Learning Management</li>
                        <li><i class="ri-check-line" style="color: #17e465;"></i>Reward & Punishment</li>
                    </ul>
                    <div class="btn-wrap">
                        <a href="#" class="btn-buy">Buy Now</a>
                    </div>
                </div>
            </div>

        </div>

    </div>
</section>
